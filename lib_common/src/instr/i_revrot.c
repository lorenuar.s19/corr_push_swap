/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   i_revrot.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/17 22:06:05 by lorenuar          #+#    #+#             */
/*   Updated: 2021/03/19 16:20:30 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libcommon.h"

int	i_revrot_a(t_stack **sta, t_stack **stb)
{
	(void)stb;
	if (!sta || !*sta || !(*sta)->next)
		return (0);
	return (stack_rev_rotate(sta));
}

int	i_revrot_b(t_stack **sta, t_stack **stb)
{
	(void)sta;
	if (!stb || !*stb || !(*stb)->next)
		return (0);
	return (stack_rev_rotate(stb));
}

int	i_revrot_both(t_stack **sta, t_stack **stb)
{
	if (!sta || !*sta || !stb || !*stb)
		return (0);
	return (i_revrot_a(sta, stb) || i_revrot_b(sta, stb));
}
