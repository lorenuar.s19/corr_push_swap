/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   i_push.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/17 22:06:05 by lorenuar          #+#    #+#             */
/*   Updated: 2021/04/04 00:52:09 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libcommon.h"

int	i_push_a(t_stack **sta, t_stack **stb)
{
	if (!stb || !*stb)
		return (0);
	if (!stack_push_data(sta, (*stb)->data))
		return (1);
	stack_pop(stb);
	return (0);
}

int	i_push_b(t_stack **sta, t_stack **stb)
{
	if (!sta || !*sta)
		return (0);
	if (!stack_push_data(stb, (*sta)->data))
		return (1);
	stack_pop(sta);
	return (0);
}
