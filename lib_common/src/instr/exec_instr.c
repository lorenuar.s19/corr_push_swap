/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_instr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/17 21:38:09 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/04 15:28:09 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libcommon.h"

int	exec_instr(t_stack **sta, t_stack **stb, int instr, int verbose)
{
	int			ret;
	static	int	(*instr_table[N_INSTR])(t_stack **, t_stack **) = {
		i_swap_a, i_swap_b, i_swap_both,
		i_push_a, i_push_b,
		i_rot_a, i_rot_b, i_rot_both,
		i_revrot_a, i_revrot_b, i_revrot_both};

	if (instr < 0 || instr > N_INSTR - 1)
	{
		return (error_printf(1, E_EI0 " : %d", instr));
	}
	ret = instr_table[instr](sta, stb);
	print_stacks(*sta, *stb, instr, verbose);
	return (ret);
}
