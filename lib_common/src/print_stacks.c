/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_stacks.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/17 17:49:45 by lorenuar          #+#    #+#             */
/*   Updated: 2021/04/04 00:51:32 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libcommon.h"

static void	sub_print_instr(int instr)
{
	static char	*str_instr_desc[N_INSTR] = {
		"swap A", "swap B", "swap A & B",
		"push a", "push b",
		"rotate a", "rotate b", "rotate A & B",
		"reverse rotate A", "reverse rotate B", "reverse rotate A & B"};

	if (instr == I_NVALID)
	{
		put_str("\n");
		return ;
	}
	ft_printf(" EXEC : %s\n", str_instr_desc[instr]);
}

static void	sub_print_index(t_pdata *pdat)
{
	if (pdat->siz_a >= pdat->siz_b)
	{
		ft_printf("%7d > ", pdat->ia);
	}
	else
	{
		ft_printf("%*d > ", pdat->pad_n - 2, pdat->ib);
	}
}

static void	sub_print_stacks(t_pdata *pdat)
{
	static int	ret = 0;
	int			a_printed;

	a_printed = 0;
	if ((*pdat->sta) && pdat->ia >= 0)
	{
		ret = ft_printf("A %-4d", (*pdat->sta)->data);
		(*pdat->sta) = (*pdat->sta)->next;
		a_printed = 1;
	}
	if (pdat->siz_b)
	{
		if (ret < pdat->pad_b)
			ret = pdat->pad_b - ret;
		if (a_printed == 0)
			ret = pdat->pad_b;
		ft_printf(" %*s| ", ret, "");
	}
	if ((*pdat->stb) && pdat->ib >= 0)
	{
		ft_printf("B %-4d ", (*pdat->stb)->data);
		(*pdat->stb) = (*pdat->stb)->next;
	}
}

void	print_stacks(t_stack *a, t_stack *b, int instr, int verbose)
{
	t_pdata	pdat;

	if (verbose == 0 || (!a && !b))
		return ;
	pdat = (t_pdata){&a, &b, 0, 0, stack_get_size(a), stack_get_size(b), 0, 0};
	pdat.pad_n = ft_printf("= - POS -");
	pdat.pad_b = ft_printf(" A size %d ", pdat.siz_a) - 2;
	ft_printf("- B size %d =", pdat.siz_b);
	sub_print_instr(instr);
	while (a || b)
	{
		sub_print_index(&pdat);
		sub_print_stacks(&pdat);
		if (verbose == 2 && (pdat.ia >= SHORT_MAX_NODES - 1
				|| pdat.ib >= SHORT_MAX_NODES - 1))
		{
			ft_printf("\t ... SHORTENED ... MAX %02d ...\n", SHORT_MAX_NODES);
			return ;
		}
		put_str("\n");
		pdat.ia++;
		pdat.ib++;
	}
	put_str("\n");
}
