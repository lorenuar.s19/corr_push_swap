/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stacks_free_all.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/17 15:54:14 by lorenuar          #+#    #+#             */
/*   Updated: 2021/04/04 00:49:49 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libcommon.h"

void	stacks_free_all(t_stack **a, t_stack **b)
{
	if (a && *a)
	{
		stack_free(a);
		*a = NULL;
	}
	if (b && *b)
	{
		stack_free(b);
		*b = NULL;
	}
}
