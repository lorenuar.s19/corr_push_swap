/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_check_sorted.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/19 14:07:08 by lorenuar          #+#    #+#             */
/*   Updated: 2021/05/19 13:45:37 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libcommon.h"

int	stack_check_sorted_ascending(t_stack *sta)
{
	int		diff;

	if (!sta)
		return (0);
	diff = 0;
	while (sta && sta->next)
	{
		if (sta->data > sta->next->data)
		{
			diff++;
		}
		sta = sta->next;
	}
	if (diff)
	{
		return (diff);
	}
	return (diff);
}

int	stack_check_sorted_descending(t_stack *sta)
{
	int		diff;

	diff = 0;
	if (!sta)
		return (0);
	while (sta && sta->next)
	{
		if (sta->data < sta->next->data)
		{
			diff++;
		}
		sta = sta->next;
	}
	if (diff)
	{
		return (diff);
	}
	return (diff);
}
