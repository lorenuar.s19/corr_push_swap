/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libcommon.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/17 15:50:11 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/04 15:24:08 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBCOMMON_H
# define LIBCOMMON_H

# define DELAY 1000

# include "libutils.h"

enum	e_instructions
{
	I_NVALID = -1,
	I_SA = 0,
	I_SB,
	I_SS,
	I_PA,
	I_PB,
	I_RA,
	I_RB,
	I_RR,
	I_RRA,
	I_RRB,
	I_RRR,
};

# define N_INSTR 11

int		stacks_parse_args(int argc, char *argv[], t_stack **a);
void	stacks_free_all(t_stack **a, t_stack **b);

int		stack_check_valid(t_stack *sta);

int		stack_check_sorted_ascending(t_stack *sta);
int		stack_check_sorted_descending(t_stack *sta);

int		read_instrs(int fd);
int		exec_instr(t_stack **sta, t_stack **stb, int instr,
			int verbose);

int		i_swap_a(t_stack **sta, t_stack **stb);
int		i_swap_b(t_stack **sta, t_stack **stb);
int		i_swap_both(t_stack **sta, t_stack **stb);
int		i_push_a(t_stack **sta, t_stack **stb);
int		i_push_b(t_stack **sta, t_stack **stb);
int		i_rot_a(t_stack **sta, t_stack **stb);
int		i_rot_b(t_stack **sta, t_stack **stb);
int		i_rot_both(t_stack **sta, t_stack **stb);
int		i_revrot_a(t_stack **sta, t_stack **stb);
int		i_revrot_b(t_stack **sta, t_stack **stb);
int		i_revrot_both(t_stack **sta, t_stack **stb);

typedef struct s_print_data
{
	t_stack		**sta;
	t_stack		**stb;
	ssize_t		ia;
	ssize_t		ib;
	ssize_t		siz_a;
	ssize_t		siz_b;
	int			pad_b;
	int			pad_n;
}				t_pdata;

# define SHORT_MAX_NODES 5

void	print_stacks(t_stack *a, t_stack *b, int instr, int verbose);

int		parse_opts(char argc, char *argv[]);

/*
** Errors
*/

# define E_SPA0 "stacks_parse_args : error from stack_from_str()"
# define E_SPA1 "stacks_parse_args : error from stack_from_args()"
# define E_PI0 "parse_instr : Instruction not found"
# define E_RI0 "read_instr : Invalid string content"
# define E_SCS0 "stack_check_sorted : input is NULL"
# define E_SCV0 "stack_check_valid : input is NULL"
# define E_SCV1 "stack_check_valid : node is out of integer range"
# define E_SPS0 "stacks_parse_args : stack is empty"
# define E_EI0 "exec_instr : instruction out of range"

#endif
