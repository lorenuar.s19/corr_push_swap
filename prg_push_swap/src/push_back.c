/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_back.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/12 20:56:04 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/04 16:00:40 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

// #define NODBG //TODO REMOVE BEFORE PUSH

#include "push_swap.h"

static int	select_best_move(t_sdat *sd, t_bmdat *bd)
{
	if (can_insert(sd, bd))
		return (error_put(1, "select_best_move : can_insert()"));
	if (sd->push_type)
	{
		sd->best_push_type = sd->push_type;
	}
	if (bd->moves.a < bd->best_moves.a)
	{
		bd->best_moves = bd->moves;
	}
	if (bd->moves.b < bd->best_moves.b)
	{
		bd->best_moves = bd->moves;
	}
	return (0);
}

static int	scan_for_best_move(t_sdat *sd, t_bmdat *bd)
{
	bd->best_moves = sd->siz;
	sd->best_push_type = 0;
	bd->pos.b = sd->siz.a;
	while (sd->stb && *(sd->stb) && bd->pos.b < sd->inp_siz
		&& bd->best_moves.a > 0)
	{
		bd->pos.a = 0;
		if (select_best_move(sd, bd))
		{
			return (error_put(1, "scan_for_best_move : select_best_move()"));
		}
		bd->pos.b++;
	}
	return (0);
}

int	push_back(t_sdat *sd, int verbose)
{
	t_bmdat	bd;

	sd->best_push_type = 1;
	while (sd->sta && (*sd->sta) && sd->stb && *(sd->stb))
	{
		bd = (t_bmdat){(t_pair){0, 0}, (t_pair){0, 0}, (t_pair){0, 0},
			(t_range){0, 0}, (t_range){0, 0}, 0};
		if (update_pos_table(sd))
			return (1);
		if (sd->stb && *(sd->stb) && scan_for_best_move(sd, &bd))
			return (1);
		if (exec_rotate(sd, &bd, verbose))
			return (1);
		if (sd->best_push_type == 1 && sd->stb && *(sd->stb)
			&& sd->best_push_type && exec_push(sd, verbose))
		{
			return (error_put(1, "scan_for_best_move : exec_push()"));
		}
		else if (sd->best_push_type == 0)
			break ;
	}
	if (sd->stb && (*sd->stb))
		return (error_put(1, "B IS NOT EMPTY after push_back"));
	return (0);
}
