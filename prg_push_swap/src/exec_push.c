/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_push.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/12 20:56:04 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/04 15:52:25 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	exec_push(t_sdat *sd, int verbose)
{
	if ((sd->sta && !(*sd->sta)) || (sd->stb && !(*sd->stb)))
	{
		return (error_put(1, "exec_push : invalid input"));
	}
	if (exec_print_instr(sd, I_PA, verbose))
		return (error_put(1, "exec_push : exec_print_instr() PA"));
	if (exec_print_instr(sd, I_RA, verbose))
		return (error_put(1, "exec_push : exec_print_instr() RA"));
	return (0);
}
