/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   can_swap.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/21 14:53:01 by lorenuar          #+#    #+#             */
/*   Updated: 2021/05/29 06:18:49 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	can_swap(t_sdat *sd, t_stack *stack, int verbose)
{
	int	pos;
	int	next_pos;

	pos = -1;
	if (stack)
	{
		pos = get_pos(sd, stack->data);
	}
	if (pos < 0)
		return (error_put(1, E_CS0));
	next_pos = -1;
	if (stack && stack->next)
	{
		next_pos = get_pos(sd, stack->next->data);
	}
	if (next_pos < 0)
		return (error_put(1, E_CS1));
	if (pos == next_pos + 1)
	{
		if (exec_print_instr(sd, I_SA, verbose))
			return (error_put(1, E_CS2));
	}
	return (0);
}
