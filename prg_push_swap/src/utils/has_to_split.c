/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   has_to_split.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/21 14:53:01 by lorenuar          #+#    #+#             */
/*   Updated: 2021/05/21 15:00:33 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	has_to_split(t_sdat *sd, t_stack *stack)
{
	int		has_to_split;

	has_to_split = 0;
	while (stack)
	{
		if (get_move_to_b(sd, stack->data))
			has_to_split = 1;
		stack = stack->next;
	}
	return (has_to_split);
}
