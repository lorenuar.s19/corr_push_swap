/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setup_pos_table.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/27 23:09:25 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/04 16:06:48 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <limits.h>
#include "push_swap.h"

static int	create_pos_table(t_ptab **itab, size_t siz_postab)
{
	size_t	i;

	if (!itab)
		return (1);
	(*itab) = malloc(siz_postab * sizeof(t_ptab));
	i = 0;
	while (i < siz_postab)
	{
		(*itab)[i].pos = -1;
		(*itab)[i].number = 0;
		(*itab)[i].to_b = 0;
		i++;
	}
	return (0);
}

static void	sub_set_min_max(t_sdat *sd, t_stack	*sta, int i)
{
	if (sta->data > sd->max)
	{
		sd->i_max = i;
		sd->max = sta->data;
	}
	if (sta->data < sd->min)
	{
		sd->i_min = i;
		sd->min = sta->data;
	}
}

static int	get_min_max(t_sdat *sd)
{
	t_stack	*sta;
	int		i;

	if (!sd->sta || !*(sd->sta))
		return (1);
	sta = *(sd->sta);
	sd->max = INT_MIN;
	sd->min = INT_MAX;
	i = 0;
	while (i < sd->inp_siz && sta)
	{
		sub_set_min_max(sd, sta, i);
		sta = sta->next;
		if (!sta && sd->stb && *(sd->stb))
		{
			sta = *(sd->stb);
		}
		i++;
	}
	return (0);
}

int	setup_pos_table(t_sdat *sd)
{
	if (create_pos_table(&(sd->postab), sd->inp_siz))
		return (error_put(1, E_SIT0));
	if (get_min_max(sd))
		return (error_put(1, E_SIT1));
	if (populate_pos_table(sd))
		return (error_put(1, E_SIT2));
	return (0);
}

int	update_pos_table(t_sdat *sd)
{
	int	i;

	sd->siz.a = 0;
	if (sd->sta && *(sd->sta))
	{
		sd->siz.a = stack_get_size(*(sd->sta));
	}
	sd->siz.b = 0;
	if (sd->stb && *(sd->stb))
	{
		sd->siz.b = stack_get_size(*(sd->stb));
	}
	i = 0;
	while (i < sd->inp_siz)
	{
		sd->postab[i].pos = -1;
		i++;
	}
	if (get_min_max(sd))
		return (error_put(1, E_SIT1));
	if (populate_pos_table(sd))
		return (error_put(1, E_UPT0));
	return (0);
}
