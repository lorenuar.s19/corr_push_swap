/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   populate_pos_table.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/10 00:15:54 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/04 16:06:27 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include "push_swap.h"

static int	fill_numbers_from_stack(t_sdat *sd)
{
	t_stack	*stack;
	int		i;

	if (!sd || !sd->sta || !sd->stb)
		return (error_put(1, E_FNFS0));
	stack = NULL;
	if (sd->sta && *(sd->sta))
		stack = *(sd->sta);
	i = 0;
	while (i < sd->inp_siz && stack)
	{
		sd->postab[i].number = stack->data;
		stack = stack->next;
		i++;
	}
	if (sd->stb && *(sd->stb))
		stack = *(sd->stb);
	while (i < sd->inp_siz && stack)
	{
		sd->postab[i].number = stack->data;
		stack = stack->next;
		i++;
	}
	return (0);
}

static int	postab_check_valid(t_sdat *sd)
{
	int	i;

	i = 0;
	while (i < sd->inp_siz)
	{
		if (sd->postab[i].pos == -1)
		{
			return (
				error_printf(1, E_PCV0 " : postab[i %d] %d", i, sd->postab[i]));
		}
		i++;
	}
	return (0);
}

static int	find_next_number(t_sdat *sd, int curr_max, int *index)
{
	int	temp;
	int	local_max;
	int	i;

	i = 0;
	local_max = INT_MIN;
	while (i < sd->inp_siz)
	{
		temp = sd->postab[i].number;
		if (local_max <= temp && temp < curr_max)
		{
			local_max = temp;
			*index = i;
		}
		i++;
	}
	return (local_max);
}

int	populate_pos_table(t_sdat *sd)
{
	int		i;
	int		pos;
	int		next;

	if (fill_numbers_from_stack(sd))
	{
		return (error_put(1, E_PPT0));
	}
	i = 0;
	next = find_next_number(sd, sd->max + 1, &i);
	pos = sd->inp_siz - 1;
	while (i >= 0 && i < sd->inp_siz && pos >= 0)
	{
		sd->postab[i].pos = pos;
		pos--;
		next = find_next_number(sd, next, &i);
	}
	return (postab_check_valid(sd));
}
