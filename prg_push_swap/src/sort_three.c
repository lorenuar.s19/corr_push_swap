/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_three.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/04 15:02:12 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/04 15:29:54 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	sort_three(t_sdat *sd, int verbose)
{
	size_t	siz;

	siz = 0;
	if (sd->sta && (*sd->sta))
		siz = stack_get_size(*(sd->sta));
	while (siz < 4 && sd->sta && *(sd->sta)
		&& stack_check_sorted_ascending(*(sd->sta)))
	{
		if (sd->sta && (*sd->sta) && (*sd->sta)->next
			&& (*sd->sta)->data > (*sd->sta)->next->data)
		{
			if (exec_print_instr(sd, I_SA, verbose))
				return (1);
		}
		if (sd->sta && (*sd->sta) && (*sd->sta)->next && (*sd->sta)->next->next
			&& (*sd->sta)->next->data > (*sd->sta)->next->next->data)
		{
			if (exec_print_instr(sd, I_RRA, verbose))
				return (1);
		}
	}
	return (0);
}
