/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   split_to_b.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/21 14:53:01 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/04 16:12:03 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	sub_split_to_b(t_sdat *sd, int verbose, int next)
{
	if (sd->sta && *(sd->sta) && in_range(sd->range, sd->postab[next].pos))
	{
		if (exec_print_instr(sd, I_PB, verbose))
			return (error_put(1, E_STB0));
	}
	else
	{
		if (exec_print_instr(sd, I_RA, verbose))
			return (error_put(1, E_STB0));
	}
	return (0);
}

int	split_to_b(t_sdat *sd, int verbose)
{
	int	next;

	next = 0;
	if (sd->stb && (*sd->stb))
		return (error_put(1, "split_to_b : cannot split, b not empty"));
	while (sd->inp_siz > 5 && next < sd->inp_siz)
	{
		if (sub_split_to_b(sd, verbose, next))
			return (1);
		next++;
	}
	if (sort_three(sd, verbose))
		return (error_put(1, E_STB3));
	if (sort_five(sd, verbose))
		return (error_put(1, "split_to_b : sub_sort_five()"));
	return (0);
}
