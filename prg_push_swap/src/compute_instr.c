/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   compute_instr.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/12 20:56:04 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/04 16:07:10 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	pos_table_reset_split(t_sdat *sd)
{
	int	i;

	i = 0;
	while (i < sd->inp_siz)
	{
		sd->postab[i].to_b = 0;
		i++;
	}
}

static int	sub_compute_instr(t_sdat *sd, int verbose, int *sorted)
{
	if (split_to_b(sd, verbose))
	{
		return (error_put(1, E_CI1));
	}
	if (push_back(sd, verbose))
	{
		return (error_put(1, E_CI2));
	}
	pos_table_reset_split(sd);
	if (update_pos_table(sd))
	{
		return (1);
	}
	*sorted = check_pre_sorted(sd);
	return (0);
}

int	compute_instr(t_sdat *sd, int verbose)
{
	int	sorted;
	int	range_i;

	range_i = 0;
	sorted = 0;
	while (sd->sta && *(sd->sta) && sorted == 0)
	{
		if (range_i < sd->range_i_max)
			sd->range = sd->range_table[range_i++];
		else
			range_i = 0;
		if (sub_compute_instr(sd, verbose, &sorted))
			return (1);
		if (sorted)
		{
			if (put_zero_on_top(sd, verbose))
				return (1);
			break ;
		}
	}
	if (stack_check_sorted_ascending((*sd->sta)))
		return (error_put(1, "STACK NOT SORTED"));
	return (0);
}
