/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   can_insert.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/21 14:53:01 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/04 15:59:04 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	sub_get_min(t_sdat *sd, t_bmdat *bd)
{
	int	pos;
	int	i;

	i = 0;
	bd->best_pos = (t_range){-1, -1};
	bd->best = (t_range){sd->inp_siz, 0};
	while (bd->pos.b + i < sd->inp_siz)
	{
		pos = sd->postab[bd->pos.b + i].pos;
		if (pos < bd->best.min)
		{
			bd->best.min = pos;
			bd->best_pos.min = i;
		}
		i++;
	}
	bd->pos.b = bd->best_pos.min;
}

int	can_insert(t_sdat *sd, t_bmdat *bd)
{
	sub_get_min(sd, bd);
	bd->moves = (t_pair){bd->pos.a, bd->pos.b};
	sd->push_type = 1;
	return (0);
}
