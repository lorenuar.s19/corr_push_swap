/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_pre_sorted.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/12 20:56:04 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/04 16:03:35 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	sub_check_under_max(t_sdat *sd, int i, int maxpos, int *not_sorted)
{
	if (sd->postab[i].pos == maxpos - 1)
	{
		if (sd->postab[i + 1].pos != 0)
		{
			*not_sorted = 1;
		}
	}
	else if (sd->postab[i].pos > sd->postab[i + 1].pos)
	{
		*not_sorted = 1;
	}
}

static void	sub_check_equal_max(t_sdat *sd, int i, int maxpos, int *not_sorted)
{
	if (sd->postab[i].pos == maxpos - 1)
	{
		if (sd->postab[0].pos != 0)
		{
			*not_sorted = 1;
		}
	}
	else if (sd->postab[i].pos > sd->postab[0].pos)
	{
		*not_sorted = 1;
	}
}

static int	sub_check_pre_sorted(t_sdat *sd, int i, int maxpos)
{
	int	not_sorted;
	int	zero_visited;

	not_sorted = 0;
	zero_visited = 0;
	while (not_sorted == 0)
	{
		if (i < maxpos - 1)
			sub_check_under_max(sd, i, maxpos, &not_sorted);
		else if (i == maxpos - 1)
			sub_check_equal_max(sd, i, maxpos, &not_sorted);
		if (sd->postab[i].pos == 0)
		{
			zero_visited++;
		}
		i++;
		if (i >= maxpos)
			i = 0;
		if (zero_visited > 1)
			break ;
	}
	return (!not_sorted);
}

int	check_pre_sorted(t_sdat *sd)
{
	int	sorted;
	int	maxpos;
	int	i;

	maxpos = sd->inp_siz;
	i = 0;
	while (i < maxpos)
	{
		if (sd->postab[i].pos == 0)
			break ;
		i++;
	}
	sorted = sub_check_pre_sorted(sd, i, maxpos);
	return (sorted);
}
