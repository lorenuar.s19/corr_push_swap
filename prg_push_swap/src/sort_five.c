/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_five.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/04 15:02:12 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/04 15:50:53 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	sub_rotate(t_sdat *sd, int verbose, int moves)
{
	while (moves)
	{
		if (moves > 0)
		{
			if (exec_print_instr(sd, I_RA, verbose))
				return (1);
			moves--;
		}
		else if (moves < 0)
		{
			if (exec_print_instr(sd, I_RRA, verbose))
				return (1);
			moves++;
		}
	}
	return (0);
}

static int	sub_move(t_sdat *sd, int verbose)
{
	int	pos;
	int	moves;

	moves = 0;
	while (moves < sd->inp_siz && sd->postab)
	{
		pos = sd->postab[moves].pos;
		if (pos > sd->inp_siz / 2)
		{
			break ;
		}
		moves++;
	}
	if (moves > sd->inp_siz / 2)
		moves = moves - sd->inp_siz;
	if (sub_rotate(sd, verbose, moves))
		return (error_put(1, "sub_get_moves : sub_rotate()"));
	return (0);
}

int	sort_five(t_sdat *sd, int verbose)
{
	size_t	siz;

	siz = 0;
	if (sd->sta && (*sd->sta))
		siz = stack_get_size(*(sd->sta));
	while (siz <= 5 && sd->sta && (*sd->sta)
		&& stack_check_sorted_ascending(*(sd->sta)))
	{
		if (sub_move(sd, verbose))
			return (1);
		if (update_pos_table(sd))
			return (error_put(1, "sub_sort_five : update_pos_table()"));
		if (sd->postab[0].pos > sd->inp_siz / 2)
			if (exec_print_instr(sd, I_PB, verbose))
				return (1);
		if (sort_three(sd, verbose))
			return (1);
	}
	return (0);
}
