/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mark_to_split.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/21 14:53:01 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/04 16:08:41 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	sub_set_pos_prev(t_sdat *sd, t_fcp *mdat, int pos)
{
	mdat->prev = -1;
	if (pos == 0)
	{
		mdat->prev = sd->postab[mdat->max].pos;
	}
	else if (pos > 0)
	{
		mdat->prev = sd->postab[pos - 1].pos;
	}
	if (mdat->prev < 0)
	{
		return (error_put(1, "sub_set_pos : mdat->prev invalid"));
	}
	return (0);
}

static int	sub_set_pos_next(t_sdat *sd, t_fcp *mdat, int pos)
{
	mdat->next = -1;
	if (pos == mdat->max)
	{
		mdat->next = sd->postab[0].pos;
	}
	else if (pos < mdat->max)
	{
		mdat->next = sd->postab[pos + 1].pos;
	}
	if (mdat->next < 0)
	{
		return (error_put(1, "sub_set_pos : mdat->next invalid"));
	}
	return (0);
}

static int	sub_set_pos(t_sdat *sd, t_fcp *mdat, int pos)
{
	if (sub_set_pos_prev(sd, mdat, pos))
	{
		return (error_put(1, "sub_set_pos : sub_set_pos_prev()"));
	}
	mdat->curr = sd->postab[pos].pos;
	if (mdat->curr < 0)
	{
		return (error_put(1, "sub_set_pos : mdat->curr invalid"));
	}
	if (sub_set_pos_next(sd, mdat, pos))
	{
		return (error_put(1, "sub_set_pos : sub_set_pos_next()"));
	}
	return (0);
}

static int	sub_set_to_split(t_sdat *sd, t_fcp *m, int pos)
{
	if (m->curr >= sd->range.min && m->curr <= sd->range.max)
	{
		sd->postab[pos].to_b = 1;
		return (0);
	}
	if (pos < 0 || pos > sd->inp_siz)
	{
		return (error_put(1, "sub_set_to_split : pos invalid"));
	}
	return (0);
}

int	mark_to_split(t_sdat *sd)
{
	t_fcp	md;
	int		pos;

	md = (t_fcp){0, 0, 0, sd->inp_siz - 1};
	pos = 0;
	while (pos >= 0 && pos < sd->inp_siz)
	{
		if (sub_set_pos(sd, &md, pos))
		{
			return (error_put(1, E_MTS0));
		}
		if (sub_set_to_split(sd, &md, pos))
			return (error_put(1, "mark_to_split : sub_set_to_split()"));
		pos++;
	}
	return (0);
}
