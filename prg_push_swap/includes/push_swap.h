/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/10 13:38:11 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/04 16:04:06 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "libutils.h"
# include "libcommon.h"

typedef struct s_pair
{
	int		a;
	int		b;
}	t_pair;

typedef struct s_find_correct_place
{
	int		prev;
	int		curr;
	int		next;
	int		max;
}	t_fcp;

typedef struct s_range
{
	int		min;
	int		max;
}				t_range;

typedef struct s_scan_for_best_move_data
{
	t_pair	pos;
	t_pair	moves;
	t_pair	best_moves;
	t_range	best_pos;
	t_range	best;
	int		mid;
}	t_bmdat;

typedef struct s_pos_table
{
	int			pos;
	int			number;
	int			to_b;
}				t_ptab;

typedef struct s_sorter_data
{
	t_stack		**sta;
	t_stack		**stb;

	t_range		*range_table;
	t_range		range;
	int			range_i_max;

	t_ptab		*postab;
	t_pair		siz;
	int			inp_siz;

	int			min;
	int			max;
	int			i_max;
	int			i_min;

	int			push_type;
	int			best_push_type;

}				t_sdat;

void	print_instr(int instr);
int		exec_print_instr(t_sdat *sd, int instr, int verbose);

int		generate_instr(t_stack **a, t_stack **b, int verbose);
int		compute_instr(t_sdat *sd, int verbose);

void	free_sorter_data(t_sdat *sd);

t_range	*create_range_table(t_sdat *sd);
int		setup_pos_table(t_sdat *sd);
int		populate_pos_table(t_sdat *sd);
int		update_pos_table(t_sdat *sd);
void	pos_table_reset_split(t_sdat *sd);

int		push_back(t_sdat *sd, int verbose);
int		exec_rotate(t_sdat *sd, t_bmdat *bd, int verbose);
int		exec_push(t_sdat *sd, int verbose);
int		check_pre_sorted(t_sdat *sd);
int		put_zero_on_top(t_sdat *sd, int verbose);

int		can_swap(t_sdat *sd, t_stack *stack, int verbose);
int		can_insert(t_sdat *sd, t_bmdat *bd);

int		get_index_num(t_sdat *sd, ssize_t num);
int		get_index_pos(t_sdat *sd, ssize_t num);

int		get_pos(t_sdat *sd, ssize_t num);

int		get_move_to_b(t_sdat *sd, ssize_t num);
int		has_to_split(t_sdat *sd, t_stack *stack);

int		mark_to_split(t_sdat *sd);
int		split_to_b(t_sdat *sd, int verbose);
int		sort_three(t_sdat *sd, int verbose);
int		sort_five(t_sdat *sd, int verbose);

int		in_range(t_range range, int num);

/*
** Errors
*/
# define E_CI0      "compute_instr : mark_to_split()"
# define E_CI1      "compute_instr : split_to_b()"
# define E_CI2      "compute_instr : scan_for_best_move()"
# define E_CRT0     "create_range_table : NULL pointer from malloc()"
# define E_CRT1     "create_range_table : Trying to divide by ZERO"
# define E_CS0      "can_swap : pos invalid"
# define E_CS1      "can_swap : next_pos invalid"
# define E_CS2      "can_swap : exec_print_instr()"
# define E_FNFS0    "fill_numbers_from_stack : NULL input"
# define E_GI0      "generate_instr : init_sorter_data()"
# define E_GI1      "generate_instr : compute_instr()"
# define E_ISD0     "init_sorter_data : create_range_table()"
# define E_ISD1     "init_sorter_data : setup_pos_tables()"
# define E_MTS0     "mark_to_split : pos invalid"
# define E_MTS1     "mark_to_split : index invalid"
# define E_PA0      "parse_args : stack_from_str()"
# define E_PA1      "parse_args : stack_from_args()"
# define E_PCV0     "postab_check_valid : found unused index"
# define E_PPT0     "setup_pos_table : fill_numbers_from_stack()"
# define E_PS0      "push_swap : parse_args()"
# define E_PS1      "push_swap : input is invalid"
# define E_PS2      "push_swap : non zero exit code from generate_instr()"
# define E_SIT0     "setup_pos_table : create_pos_table()"
# define E_SIT1     "setup_pos_table : get_min_max()"
# define E_SIT2     "setup_pos_table : populate_pos_table()"
# define E_SMTSSP0  "sub_mark_to_split_set_pos : pos invalid"
# define E_SMTSSP1  "sub_mark_to_split_set_pos : next_pos invalid"
# define E_STB0     "split_to_b : exec_print_instr() push"
# define E_STB1     "split_to_b : exec_print_instr() rotate"
# define E_STB2     "split_to_b : can_swap()"
# define E_STB3     "split_to_b : sub_sort_three()"
# define E_UPT0     "update_pos_tables : populate_pos_table()"

#endif
